//
//  StoryViewController.swift
//  LiveStory
//
//  Created by Filip Bellander on 02/04/17.
//  Copyright © 2017 Filip Bellander. All rights reserved.
//

import UIKit

class StoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var Reaction: UILabel!
    @IBOutlet weak var StoryText: UILabel!
    
    @IBOutlet weak var ChoiseTable: UITableView!
    
    var tile = 0
    var nextStory = 0
    var alive = true
    var storyCount = 0
    var opTile = 0
    
    //TODO DIFFERENT ARRAYS PER CHOICE 1/2/3 = easier to build
    
    var arr: [[String]] =
        [
            ["You are sitting in a car driving and see two yellow refelecting eyes on the road", "Drive faster","Slow down the car","Stop the vehicle"],
            ["You drive by the yellow eyes. Wondering the trip home what the thing was","OK"],
            
            
            ["You finally made it home safe and sound, but like a blixt from the backmirror you notice the yellow eyes","Go out of the car","Stay in the car","Call wife"],
            ["You walk fast paced to the front door, feeling a bit troubled about the moment","ok"],
            
            
            
            
            ["Having the keys in your hand you penetrate the lock.                      *BOOOM", "Walk in to the hall","Stay outside"],
            
            //["DEAD5"],
            ["Walking into the hall with your shoes still on you switch the light on.", "OK"],
            
            ["You go searching for the loud sound you heard before walking in the frontdoor.                    Wandering slowly forwards you walk through the hall and open the first door to the right", "OK"],
            
            
            ["You see something so disgusting, Round, Black Blob with yellow eyes, Shivering your soul.              *Splat you shat your pants","Run to the kitchen", "Attack the monster"],
            
            //["DEAD6"],
            ["Running to the kitchen looking back you see a black blob following the walls, Yellow eyes stalking you", "Ok"],
            
            
            ["Inside the kitchen you grab the first thing you see","Knife"],
            
            
            ["Sweat dropping down your eyebrow makes your vision blurry", "OK"],
            
            
            ["Face to face with Yellow EYE you make a last effort for you life", "Throw knife at Yellow", "Stab Yellow in the Eye"],
            
            
            //["Blurry vision seeing Yellow flay around the room Bouncing from wall to wall. It leaves a stench like rotten fish, Blood dark as the Night","You faint"],
            ["Throwing the knife was useless. YelloW put out a Mighty ROAR shaking and making me coward into my own arms","THE END"],
            
            
            
            
            ["Waking up you see your wife dead on the floor, WHAT HAVE I DONE!", "THE END"]
            
        ]
    
    var arr2: [[String]] =
    [
        ["You are sitting in a car driving and see two yellow refelecting eyes on the road", "Drive faster","Slow down the car","Stop the vehicle"],
        ["DEAD1"],
        
        ["You finally made it home safe and sound, but like a blixt from the backmirror you notice the yellow eyes","Go out of the car","Stay in the car","Call wife"],
        ["DEAD3"],
        
        ["Having the keys in your hand you penetrate the lock.                      *BOOOM", "Walk in to the hall","Stay outside"],
        ["DEAD5"],
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ChoiseTable.rowHeight = UITableViewAutomaticDimension
        ChoiseTable.estimatedRowHeight = 200
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch indexPath.row {
        case 1:
            
            if arr[nextStory].count == 2
            {
                
                tile = nextStory - 1
                opTile = nextStory
                
                if nextStory > 3
                {
                    nextStory += 1
                    print(nextStory)
                    opTile = nextStory
                    
                }
                else
                {
                    nextStory += arr[nextStory].count - 1
                    print(arr[nextStory].count)
                    print("tile1", tile)
                }
                
            }
            else if arr[nextStory].count == 3
            {
                
                tile = nextStory - 1
                
                
                if nextStory > 3
                {
                    nextStory += 2
                    print(nextStory)
                    opTile = nextStory
                }
                else
                {
                    nextStory += arr[tile].count - 2
                    print(arr[nextStory].count)
                    print("tile2", tile)
                }
            }
            else if arr[nextStory].count == 4
            {
                
                if nextStory > 3
                {
                    print(nextStory)
                    nextStory += 3
                    opTile = nextStory
                }
                else
                {
                    nextStory += arr[tile].count - 1
                    print(arr[nextStory].count)
                    print("tile3", tile)
                    
                    tile = nextStory - 1
                }
            }
            
            
            
            
            //nextStory += arr[tile].count + tile
            //storyCount += arr[tile].count - 2
            //tile += 1
            
            
            
            
            //nextStory = arr[tile].count - 1
            tableView.reloadData()
            break
        case 2:
            if arr[nextStory].count == 2
            {
                
                tile = nextStory - 1
                
                
                if nextStory > 3
                {
                    nextStory += 2
                    print(nextStory)
                    opTile = nextStory
                }
                else
                {
                    nextStory += arr[nextStory].count - 1
                    print(arr[nextStory].count)
                    print("tile1", tile)
                }
                
            }
            else if arr[nextStory].count == 3
            {
                
                tile = nextStory - 1
                
                
                if nextStory > 3
                {
                    nextStory += 1
                    print(nextStory)
                    opTile = nextStory + 1
                }
                else
                {
                    nextStory += arr[tile].count - 2
                    print(arr[nextStory].count)
                    print("tile2", tile)
                }
            }
            else if arr[nextStory].count == 4
            {
                
                if nextStory > 3
                {
                    print(nextStory)
                    nextStory += 3
                    print(nextStory)
                }
                else
                {
                    nextStory += arr[tile].count - 1
                    print(arr[nextStory].count)
                    print("tile3", tile)
                    
                    tile = nextStory - 1
                }
            }
            
            tableView.reloadData()
            break
        case 3:
            tile += 3
            print(arr[3])
            tableView.reloadData()
        default:
            print("NO CASE")
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.ChoiseTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        //cell.choiceLbl.text = arr[tile][indexPath.row]
        cell.choiceLbl.text = arr[nextStory][indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr[nextStory].count
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
