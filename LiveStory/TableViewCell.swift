//
//  TableViewCell.swift
//  MR.CAL1.0
//
//  Created by Filip Bellander on 13/11/16.
//  Copyright © 2016 Filip Bellander. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var choiceLbl: UILabel!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
